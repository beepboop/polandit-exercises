#! /usr/bin/python3


def prepare_list(vals):
    """Receive string (comma seperated) or tuple and return list."""
    if isinstance(vals, tuple):
        out = list(vals)
    elif isinstance(vals, str):
        out = vals.split(",")
    return out


def main():
    A = prepare_list(input("A="))
    B = prepare_list(input("B="))

    result = {}

    for b in B:
        tmp = 0
        for a in A:
            if a is b:
                tmp += 1
        result[b] = tmp

    print(str(result))


if __name__ == "__main__":
    main()
